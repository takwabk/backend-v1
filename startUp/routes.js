const express = require("express");
const auth = require("../routes/auth");
const client = require("../routes/client");
const etat = require("../routes/etat");
const activite = require("../routes/activite");
const operation = require("../routes/operation");
const projet = require("../routes/projet");
const staff = require("../routes/staff");
const type = require("../routes/type");
const ic1 = require("../routes/IC1");
const ic2 = require("../routes/IC2");
const description = require("../routes/DescriptionBiblio");
const fiche = require("../routes/ficheComplementaire");
const role = require("../routes/role");
const donnee = require("../routes/DonneeConstruction");
const error = require("../middleware/error");

module.exports = function (app) {
  app.use(express.json({ limit: "50mb", extended: true }));
  app.use(
    express.urlencoded({ limit: "50mb", extended: true, parameterLimit: 50000 })
  );

  app.use("", auth);
  app.use("", client);

  app.use("", etat);
  app.use("", projet);
  app.use("", activite);
  app.use("", operation);
  app.use("", staff);
  app.use("", type);
  app.use("", ic1);
  app.use("", ic2);
  app.use("", description);
  app.use("", fiche);
  app.use("", role);
  app.use("", donnee);
  app.use(error); //pass refrence to the function not calling error()
};
