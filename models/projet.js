const config = require("config");
const jwt = require("jsonwebtoken");
const Joi = require("joi");
const mongoose = require("mongoose");
const string = require("joi/lib/types/string");
const projetSchema = new mongoose.Schema({

    ref:{
        type:String,
       

    },
porteurProjet:{
    type:String,
    required:true,

},
adresseProjet:{
    type:String,
    required:true,
},
interlocuteur:{
   type:String,
   required:true 
},
tel:{
    type:String,
    required:true,

},
email:{
    type:String,
    required:true,
},
descriptionProjet:{
    type:String,
    required:true,
},

createdOn: { type: Date, default: Date.now },

})
const Projet = mongoose.model("Projet", projetSchema);

function validateProjet(projet) {
  
  const schema = {
    ref:Joi.string().optional(),
    porteurProjet: Joi.string().required(),
    adresseProjet: Joi.string().required(),
    interlocuteur: Joi.string().required(),
    tel:Joi.string().required(),
    email:Joi.string().required().email(),
    descriptionProjet:Joi.string().required(),
    

    

  }
  return Joi.validate(projet, schema);
}
exports.projetSchema = projetSchema;
exports.Projet = Projet;
exports.validateProjet = validateProjet;

