var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var DonneeSchema = new Schema({

  Interet_porteur_projet: 
    {
      Etude_preleminaire_biomasse: 
        {
          SS_Traitance: String,
          marge_percent: String,
          marge_euro: String,
          marge_totale: String,
          Prix_vente_Concep: String,
         
        },

      
      Rapport_prefaisabilite: 
        {
          SS_Traitance: String,
          marge_percent: String,
          marge_euro: String,
         marge_totale: String,
          Prix_vente_Concep: String,
         
        },
      
      Impact_economique_exploitant: 
        {
          SS_Traitance: String,
          marge_percent: String,
          marge_euro: String,
         marge_totale: String,
          Prix_vente_Concep: String,
         
        },
      
      Bilan_agronomique_sols: 
        {
          SS_Traitance: String,
          marge_percent: String,
          marge_euro: String,
         marge_totale: String,
          Prix_vente_Concep: String,
         
        },
      
      Apres_Avis_favorable_Creation_SPV: 
        {
          SS_Traitance: String,
          marge_percent: String,
          marge_euro: String,
         marge_totale: String,
          Prix_vente_Concep: String,
       
        },
      
      Sous_Total_Conception: String,
      Sous_Total_Prix_vente_Concep:String,
      Marge_totaux:String,
    },
    


  Etude_biomasse_projet: 
    {
      Cibler_besoins_complementaires_nutritionnels: 
        {
          SS_Traitance: String,
          marge_percent: String,
          marge_euro: String,
         marge_totale: String,
          Prix_vente_Concep: String,
        },
      
      Rapport_etude_approfondi_pouvoir_meethanogeene: 
        {
          SS_Traitance: String,
          marge_percent: String,
          marge_euro: String,
         marge_totale: String,
          Prix_vente_Concep: String,
        },
      
      Apport_supplementaire_intrants_reseau_ENR_Agri: 
        {
          SS_Traitance: String,
          marge_percent: String,
          marge_euro: String,
          marge_totale: String,
          Prix_vente_Concep: String,
        },
      
      Assistance_juridique_Apporteur_substrat_SPV: 
        {
          SS_Traitance: String,
          marge_percent: String,
          marge_euro: String,
         marge_totale: String,
          Prix_vente_Concep: String,
        },
      
      Sous_Total_Conception: String,
      Sous_Total_Prix_vente_Concep:String,
      Marge_totaux:String,
    },
  
  Etude_fonciere_projet: 
    {
      Optimisation_emplacement_site: 
        {
          SS_Traitance: String,
          marge_percent: String,
          marge_euro: String,
         marge_totale: String,
          Prix_vente_Concep: String,
        },
      
      Releve_topographique: 
        {
          SS_Traitance: String,
          marge_percent: String,
          marge_euro: String,
         marge_totale: String,
          Prix_vente_Concep: String,
        },
      
      Rapport_geometre: 
        {
          SS_Traitance: String,
          marge_percent: String,
          marge_euro: String,
         marge_totale: String,
          Prix_vente_Concep: String,
        },
      
      Plan_masse: 
        {
          SS_Traitance: String,
          marge_percent: String,
          marge_euro: String,
         marge_totale: String,
          Prix_vente_Concep: String,
        },
      
      Analyse_geotechnique_primaire: 
        {
          SS_Traitance: String,
          marge_percent: String,
          marge_euro: String,
         marge_totale: String,
          Prix_vente_Concep: String,
        },
      
      Analyse_geotechnique_approfondie: 
        {
          SS_Traitance: String,
          marge_percent: String,
          marge_euro: String,
         marge_totale: String,
          Prix_vente_Concep: String,
        },
      
      Assistance_juridique_aupres_notaire_Proprietaire_foncier_SPV: 
        {
          SS_Traitance: String,
          marge_percent: String,
          marge_euro: String,
         marge_totale: String,
          Prix_vente_Concep: String,
        },
      
      Sous_Total_Conception: String,
      Sous_Total_Prix_vente_Concep:String,
      Marge_totaux:String,
    },
  
  sercurisation_gaziers: 
    {
      etude_detaille_reseau_transport__distribution_gaz: 
        {
          SS_Traitance: String,
          marge_percent: String,
          marge_euro: String,
         marge_totale: String,
          Prix_vente_Concep: String,
        },
      
      Convention_raccordement_gestionnaire_reseau: 
        {
          SS_Traitance: String,
          marge_percent: String,
          marge_euro: String,
         marge_totale: String,
          Prix_vente_Concep: String,
        },
      
      Contrat_vente_biomethane: 
        {
          SS_Traitance: String,
          marge_percent: String,
          marge_euro: String,
         marge_totale: String,
          Prix_vente_Concep: String,
        },
      
      Sous_Total_Conception: String,
      Sous_Total_Prix_vente_Concep:String,
      Marge_totaux:String,
    },
  
  valorisation_digestat:
    {
      etude_qualite_digestat:
        {
          SS_Traitance: String,
          marge_percent: String,
          marge_euro: String,
         marge_totale: String,
          Prix_vente_Concep: String,
        },
      
      Diagnostic_zone_epandable: 
        {
          SS_Traitance: String,
          marge_percent: String,
          marge_euro: String,
         marge_totale: String,
          Prix_vente_Concep: String,
        },
      
      Chiffrage_budget_epandage: 
        {
          SS_Traitance: String,
          marge_percent: String,
          marge_euro: String,
         marge_totale: String,
          Prix_vente_Concep: String,
        },
      
      Dossier_autorisation_plan_epandage:
        {
          SS_Traitance: String,
          marge_percent: String,
          marge_euro: String,
         marge_totale: String,
          Prix_vente_Concep: String,
        },
      
      Rendez_vous_exploitants_recevront_digestat:
        {
          SS_Traitance: String,
          marge_percent: String,
          marge_euro: String,
         marge_totale: String,
          Prix_vente_Concep: String,
        },
      
      Assistance_juridique_Contrat_epandage_SPV: 
        {
          SS_Traitance: String,
          marge_percent: String,
          marge_euro: String,
          marge_totale: String,
          Prix_vente_Concep: String,
        },
      
      Sous_Total_Conception: String,
      Sous_Total_Prix_vente_Concep:String,
    },
  
  Realisation_Marketing: 
    {
      Plaquette_presentation: 
        {
          SS_Traitance: String,
          marge_percent: String,
          marge_euro: String,
          marge_totale: String,
          Prix_vente_Concep: String,
        },
      
      Realisation_infographie_3D: 
        {
          SS_Traitance: String,
          marge_percent: String,
          marge_euro: String,
          marge_totale: String,
          Prix_vente_Concep: String,
        },
      
      Film_animation: 
        {
          SS_Traitance: String,
          marge_percent: String,
          marge_euro: String,
          marge_totale: String,
          Prix_vente_Concep: String,
        },
      
      Site_internet: 
        {
          SS_Traitance: String,
          marge_percent: String,
          marge_euro: String,
          marge_totale: String,
          Prix_vente_Concep: String,
        },
      
      Sous_Total_Conception: String,
      Sous_Total_Prix_vente_Concep:String,
    },
  
  acceptabilite_projet: 
    {
      Diagnostic_territorial: 
        {
          SS_Traitance: String,
          marge_percent: String,
          marge_euro: String,
          marge_totale: String,
          Prix_vente_Concep: String,
        },
      
      Realisation_plans_action: 
        {
          SS_Traitance: String,
          marge_percent: String,
          marge_euro: String,
          marge_totale: String,
          Prix_vente_Concep: String,
        },
      
      Film_aniFormation_riverains_bienfaits_methanisationmation: 
        {
          SS_Traitance: String,
          marge_percent: String,
          marge_euro: String,
          marge_totale: String,
          Prix_vente_Concep: String,
        },
      
      Defense_promotion_projet_elus_population_locale: 
        {
          SS_Traitance: String,
          marge_percent: String,
          marge_euro: String,
          marge_totale: String,
          Prix_vente_Concep: String,
        },
      
      Reponses_questions_enquete_publiques: 
        {
          SS_Traitance: String,
          marge_percent: String,
          marge_euro: String,
          marge_totale: String,
          Prix_vente_Concep: String,
        },
      
      Sous_Total_Conception: String,
      Sous_Total_Prix_vente_Concep:String,
    },
  
  realisation_Projet: 
    {
      Dimensionnement_Projet: 
        {
          SS_Traitance: String,
          marge_percent: String,
          marge_euro: String,
          marge_totale: String,
          Prix_vente_Concep: String,
        },
      
      etude_dispersion_odeur_eventuelle: 
        {
          SS_Traitance: String,
          marge_percent: String,
          marge_euro: String,
          marge_totale: String,
          Prix_vente_Concep: String,
        },
      
      Proposition_choix_process: 
        {
          SS_Traitance: String,
          marge_percent: String,
          marge_euro: String,
          marge_totale: String,
          Prix_vente_Concep: String,
        },
      
      Dossier_consultation_Entreprises: 
        {
          SS_Traitance: String,
          marge_percent: String,
          marge_euro: String,
          marge_totale: String,
          Prix_vente_Concep: String,
        },
      
      Contrat_TRC_TRME_Exploitation_negociee:
        {
          SS_Traitance: String,
          marge_percent: String,
          marge_euro: String,
          marge_totale: String,
          Prix_vente_Concep: String,
        },
      
      Business_Plan_detaille: 
        {
          SS_Traitance: String,
          marge_percent: String,
          marge_euro: String,
          marge_totale: String,
          Prix_vente_Concep: String,
        },
      

      Contrat_prestataire_Controle_technique:
        {
          SS_Traitance: String,
          marge_percent: String,
          marge_euro: String,
          marge_totale: String,
          Prix_vente_Concep: String,
        },
      
      Sous_Total_Conception: String,
      Sous_Total_Prix_vente_Concep:String,
    },
  
  obtention_autorisations_urbanisme_exploitation_Projet: 
    {
      Diagnostic_environnemental:
        {
          SS_Traitance: String,
          marge_percent: String,
          marge_euro: String,
          marge_totale: String,
          Prix_vente_Concep: String,
        },
      
      Obtention_agrement_sanitaire: 
        {
          SS_Traitance: String,
          marge_percent: String,
          marge_euro: String,
          marge_totale: String,
          Prix_vente_Concep: String,
        },
      
      etude_loi_eau: 
        {
          SS_Traitance: String,
          marge_percent: String,
          marge_euro: String,
          marge_totale: String,
          Prix_vente_Concep: String,
        },
      
      etude_foudre: 
        {
          SS_Traitance: String,
          marge_percent: String,
          marge_euro: String,
          marge_totale: String,
          Prix_vente_Concep: String,
        },
      
      Dossier_ICPE: 
        {
          SS_Traitance: String,
          marge_percent: String,
          marge_euro: String,
          marge_totale: String,
          Prix_vente_Concep: String,
        },
      
      Obtention_arret_PC_Passage_huissier:
        {
          SS_Traitance: String,
          marge_percent: String,
          marge_euro: String,
          marge_totale: String,
          Prix_vente_Concep: String,
        },
      

      Rendez_vous_service_instructeur: 
        {
          SS_Traitance: String,
          marge_percent: String,
          marge_euro: String,
          marge_totale: String,
          Prix_vente_Concep: String,
        },
      
      Sous_Total_Conception: String,
      Sous_Total_Prix_vente_Concep:String,
    },
  
  obtention_subvention_Projet:
    {
      Reunion_organismes:
        {
          SS_Traitance: String,
          marge_percent: String,
          marge_euro: String,
          marge_totale: String,
          Prix_vente_Concep: String,
        },
      
      Depot_dossier_subventions:
        {
          SS_Traitance: String,
          marge_percent: String,
          marge_euro: String,
          marge_totale: String,
          Prix_vente_Concep: String,
        },
      
      Sous_Total_Conception: String,
      Sous_Total_Prix_vente_Concep:String,
    },
  
  obtention_financement_Projet: 
    {
      Instruction_dossier_bancaire: 
        {
          SS_Traitance: String,
          marge_percent: String,
          marge_euro: String,
          marge_totale: String,
          Prix_vente_Concep: String,
        },
      
      Suivi_instructions_negociations: 
        {
          SS_Traitance: String,
          marge_percent: String,
          marge_euro: String,
          marge_totale: String,
          Prix_vente_Concep: String,
        },
      
      Sous_Total_Conception: String,
      Sous_Total_Prix_vente_Concep:String,
    },
    //construction
    Tab_140: {
      Ouverture_chantier: {
        Controle_technique_SPS: {
          SS_Traitance: String,
          marge_percent: String,
          marge_euro: String,
          marge_totale: String,
          Prix_vente_Concep: String,
        },

        Frais_dossier: {
          SS_Traitance: String,
          marge_percent: String,
          marge_euro: String,
          marge_totale: String,
          Prix_vente_Concep: String,
        },
        Conseil_suivi: {
          SS_Traitance: String,
          marge_percent: String,
          marge_euro: String,
          marge_totale: String,
          Prix_vente_Concep: String,
        },
        Aleas: {
          SS_Traitance: String,
          marge_percent: String,
          marge_euro: String,
          marge_totale: String,
          Prix_vente_Concep: String,
        },
        TRCME: {
          SS_Traitance: String,
          marge_percent: String,
          marge_euro: String,
          marge_totale: String,
          Prix_vente_Concep: String,
        },
        Achat_Terrain: {
          SS_Traitance: String,
          marge_percent: String,
          marge_euro: String,
          marge_totale: String,
          Prix_vente_Concep: String,
        },
        Frais_notaire: {
          SS_Traitance: String,
          marge_percent: String,
          marge_euro: String,
          marge_totale: String,
          Prix_vente_Concep: String,
        },
        Sous_Total_Conception: String,
        Sous_Total_Prix_vente_Concep: String,
      },


      //2
      Lot_1_Process_methanisation: {
        Process_methanisation: {
          SS_Traitance: String,
          marge_percent: String,
          marge_euro: String,
          marge_totale: String,
          Prix_vente_Concep: String,
        },
        Pieces_usure_premiere_urgence_Lot_1: {
          SS_Traitance: String,
          marge_percent: String,
          marge_euro: String,
          marge_totale: String,
          Prix_vente_Concep: String,
        },
        Sous_Total_Conception: String,
        Sous_Total_Prix_vente_Concep: String,

      },

      //3
      Lot_2_Valorisation: {
        Epuration_Compression_Chaudiere: {
          SS_Traitance: String,
          marge_percent: String,
          marge_euro: String,
          marge_totale: String,
          Prix_vente_Concep: String,
        },
        Cogenerateur: {
          SS_Traitance: String,
          marge_percent: String,
          marge_euro: String,
          marge_totale: String,
          Prix_vente_Concep: String,
        },
        Sous_Total_Conception: String,
        Sous_Total_Prix_vente_Concep: String,

      },
      //4
      Lot_3_Terrassement_Grande_Masse_Talutage_VRD: {
        Terrassement_Grande_Masse_Talutafe_VRD: {
          SS_Traitance: String,
          marge_percent: String,
          marge_euro: String,
          marge_totale: String,
          Prix_vente_Concep: String,
        },

        Sous_Total_Conception: String,
        Sous_Total_Prix_vente_Concep: String,
      },
      //5
      Lot_4_Genie_Civil_circulaire_digestat_liquide_couverture_simple: {
        Genie_Civil_circulaire_digestat_liquide_couverture_simple: {
          SS_Traitance: String,
          marge_percent: String,
          marge_euro: String,
          marge_totale: String,
          Prix_vente_Concep: String,
        },

        Sous_Total_Conception: String,
        Sous_Total_Prix_vente_Concep: String,
      },
      //6
      Lot_5_Genie_Civil_ouvrages_peripheriques: {
        Genie_Civil_ouvrages_peripheriques: {
          SS_Traitance: String,
          marge_percent: String,
          marge_euro: String,
          marge_totale: String,
          Prix_vente_Concep: String,
        },

        Sous_Total_Conception: String,
        Sous_Total_Prix_vente_Concep: String,
      },
      //7
      Lot_6_Charpente_Batiment_Couverture: {
        Charpente_Batiment_Couverture: {
          SS_Traitance: String,
          marge_percent: String,
          marge_euro: String,
          marge_totale: String,
          Prix_vente_Concep: String,
        },

        Sous_Total_Conception: String,
        Sous_Total_Prix_vente_Concep: String,
      },
      //8
      Lot_7_Courant_fort_Soutirage_Electricite_Generale: {
        Courant_fort_Soutirage_Electricite_Generale: {
          SS_Traitance: String,
          marge_percent: String,
          marge_euro: String,
          marge_totale: String,
          Prix_vente_Concep: String,
        },

        Sous_Total_Conception: String,
        Sous_Total_Prix_vente_Concep: String,
      },
      //9
      Maitrise_oevre: {
        Maitrise_oevre: {
          SS_Traitance: String,
          marge_percent: String,
          marge_euro: String,
          marge_totale: String,
          Prix_vente_Concep: String,
        },

        Sous_Total_Conception: String,
        Sous_Total_Prix_vente_Concep: String,

      },
      //10
      Assistance_maitrise_ouvrage: {
        AMO: {
          SS_Traitance: String,
          marge_percent: String,
          marge_euro: String,
          marge_totale: String,
          Prix_vente_Concep: String,
        },

        Sous_Total_Conception: String,
        Sous_Total_Prix_vente_Concep: String,
      },
      //11
      Materiel: {
        Matrriels_agricoles: {
          SS_Traitance: String,
          marge_percent: String,
          marge_euro: String,
          marge_totale: String,
          Prix_vente_Concep: String,
        },

        Pont_bascule_Reserve_Incendie: {
          SS_Traitance: String,
          marge_percent: String,
          marge_euro: String,
          marge_totale: String,
          Prix_vente_Concep: String,
        },
        Hygenisiation: {
          SS_Traitance: String,
          marge_percent: String,
          marge_euro: String,
          marge_totale: String,
          Prix_vente_Concep: String,
        },
        Semi_remorque_44_t: {
          SS_Traitance: String,
          marge_percent: String,
          marge_euro: String,
          marge_totale: String,
          Prix_vente_Concep: String,
        },

        Sous_Total_Conception: String,
        Sous_Total_Prix_vente_Concep: String,
      },
    }
  
});

const Donnee = mongoose.model("Donnee", DonneeSchema);


exports.DonneeSchema = DonneeSchema;
exports.Donnee = Donnee;

