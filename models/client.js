const Joi = require("joi");
const mongoose = require("mongoose");
// const Operation = require('../models/operation');
// const Activite=require('../models/activite');
const { etatSchema } = require("../models/etat");
const { activiteSchema } = require("../models/activite");
const { operationSchema } = require("../models/operation");
const { projetSchema } = require("../models/projet");
//const { optional } = require("joi/lib/types/lazy");
const { Fiche, ficheSchema } = require("../models/ficheComplementaire");

mois = ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"];
etats=["Client En Quotation","Client En Quotation","Contrat Cadre" ,"Client En Conception" 
,"Client En Construction" ,"Client En service"]
fiscale = ["03", "04", "05"];

const clientSchema = new mongoose.Schema({


  etats: {
    type: String,
    default: "Client En Quotation",
  },
  fiscale: {
    type: String,
    default: "03",
    required: true,
  },
  Src_Client: {
    type: String,
  },
  Date_src: {
    type: String,
  },
  Date_StatQ: {
    type: String,
  },
  Date_StatCC: {
    type: String,
  },
  Date_StatCp: {
    type: String,
  },
  Date_StatCs: {
    type: String,
  },
  Date_StatEs: {
    type: String,
  },

  

  matricule: {
    type: String,
    default: "",
  },
  mois: {
    type: String,
    default: "1",
  },
  prenom: {
    type: String,
  },
  nom: {
    type: String,
  },
  fonction: {
    type: String,
  },
  email: {
    type: String,
    required: true,
  },
  tel: {
    type: String,
    required: true,
  },
  adresse: {
    type: String,
  },
  ville: {
    type: String,
  },
  pays: {
    type: String,
  },

  
  tel_Domicile: {
    type: String,
  },
  complement:String,
 

  createdOn: { type: Date, default: Date.now },
});

const Client = mongoose.model("Client", clientSchema);

function validateClient(client) {
  const schema = {
    etats: Joi.string().valid(...etats),
   
    mois: Joi.string().valid(...mois),
    fiscale: Joi.string().valid(...fiscale),
    
  };
  return Joi.validate(client, schema);
}
exports.clientSchema = clientSchema;
exports.Client = Client;
exports.validateClient = validateClient;
