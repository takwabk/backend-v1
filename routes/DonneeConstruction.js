const express = require("express");
const router = express.Router();
const { Donnee } = require("../models/DonneeConstruction");

const asyncMiddleware = require("../middleware/async");

router.post(
  "/donnees",

  asyncMiddleware(async (req, res) => {
    let donnees = await Donnee.findOne();
    
      // if (donnees)
      //   return res.status(400).json({
      //     msgsrv: `already registered.`,
      //   });
    
      donnees = new Donnee(req.body);
      donnees = await donnees.save();
    
      res.send(donnees);
    }));
    router.put(
      "/updateDate/:id",
     
      asyncMiddleware(async (req, res) => {
       
        const donnees = await Donnee.findByIdAndUpdate(
          req.params.id,
          { ...req.body },
          { new: true }
        );
    
        if (!donnees)
          return res
            .status(404)
            .send("The conception with the given ID was not found.");
    
        res.send(donnees);
      })
    );


router.get(
  "/getDonnees",
  asyncMiddleware(async (req, res) => {
    const donnees = await Donnee.find();
    //  result = JSON.parse(JSON.stringify(fiche))

    res.send(donnees);
  })
);
module.exports = router;
