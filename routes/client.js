const express = require("express");
const router = express.Router();
const { Client, validateClient } = require("../models/client");
const { Etat } = require("../models/etat");
const { Operation } = require("../models/operation");
// const { Projet } = require("../models/projet");
const { Activite } = require("../models/activite");
const asyncMiddleware = require("../middleware/async");
const validateObjectId = require("../middleware/validObjectId");
const validate = require("../middleware/validateRequest");
const { Type } = require("../models/type");
const { IC1 } = require("../models/IC1");
const { IC2 } = require("../models/IC2");
const { DescriptionBiblio } = require("../models/DescriptionBiblio");
const { Fiche } = require("../models/ficheComplementaire");
const authorize = require("../middleware/role");
const authorization = require("../middleware/auth");

router.post(
  "/addClient",

  

  asyncMiddleware(async (req, res) => {
    console.log("client",req.body);
    let client = await Client.findOne({

      matricule: req.body.matricule,
    });

    if (client)
      return res.status(400).json({
        msgsrv: `${req.body.nom} with reference : ${req.body.matricule} already registered.`,
      });

   

    

    let matricule = "";
    let eneragri = "C";
    client = new Client({
      matricule: req.body.fiscale + req.body.mois + eneragri + 100,
      nom: req.body.nom,
      prenom: req.body.prenom,
      fonction: req.body.fonction,
      email: req.body.email,
      tel: req.body.tel,
      adresse: req.body.adresse,
      ville: req.body.ville,
      pays: req.body.pays,
      complement:req.body.complement,
     
      Src_Client: req.body.Src_Client,
      Date_src: req.body.Date_src,
      Date_StatQ: req.body.Date_StatQ,
      Date_StatCC: req.body.Date_StatCC,
      Date_StatCp: req.body.Date_StatCp,
      Date_StatCs: req.body.Date_StatCs,
      Date_StatEs: req.body.Date_StatEs,
      etats:req.body.etats,

     
     
      
    });
   

    const l = await Client.find().countDocuments();

    for (let i = 1; i < l + 1; i++) {
      let _matricule = {
        matricule: req.body.fiscale + req.body.mois + eneragri + `${100 + i}`,
      };

      await client.set(_matricule);
    }
    await client.save();

    res.send(client);
   
  })
);

router.get(
  "/clients",
  asyncMiddleware(async (req, res) => {
    const clients = await Client.find().sort("createdOn");
    res.send(clients);
  })
);

//
// router.get(
//   "/allNews",
//   asyncMiddleware(async (req, res) => {
//     const news = await News.find();
//     res.send(news);
//   })
// );
// router.put(
//   "/updateNews/:id",
//   [authorization, isAdmin, validateObjectId, validate(validateNews)],
//   asyncMiddleware(async (req, res) => {
//     const artiste = await Artiste.findById(req.body.artiste_id);
//     if (!artiste) return res.status(400).send("Invalid Artiste.");

//     const news = await News.findByIdAndUpdate(
//       req.params.id,
//       {
//         news_name: req.body.news_name,
//         description: req.body.description,
//         video: req.body.video,
//         photo: req.body.photo,
//         artiste: {
//           _id: artiste._id,
//           artiste_name: artiste.artiste_name,
//           short_description: artiste.short_description,
//         },
//       },
//       { new: true }
//     );

//     if (!news)
//       return res.status(404).send("The news with the given ID was not found.");

//     res.send(news);
//   })
// );

// router.delete(
//   "/deleteNews/:id",
//   [authorization, isAdmin, validateObjectId],
//   asyncMiddleware(async (req, res) => {
//     const news = await News.findByIdAndRemove(req.params.id);

//     if (!news)
//       return res.status(404).send("The news with the given ID was not found.");

//     res.send(news);
//   })
// );

router.get(
  "/:id",
  validateObjectId,

  asyncMiddleware(async (req, res) => {
    const client = await Client.findById(req.params.id);

    if (!client)
      return res
        .status(404)
        .send("The client with the given ID was not found.");

    res.send(client);
  })
);
router.put(
  "/:id",
  [validateObjectId, validate(validateClient)],

  asyncMiddleware(async (req, res) => {
    const type = await Type.findById(req.body.type_id);

    if (!type) return res.status(400).send("Invalid type.");

    console.log("tyyyype", type);

    const description = await DescriptionBiblio.findById(
      req.body.description_id
    );

    if (!description) return res.status(400).send("Invalid description.");

    const ic1 = await IC1.findById(req.body.ic1_id);

    if (!ic1) return res.status(400).send("Invalid ic1.");

    const ic2 = await IC2.findById(req.body.ic2_id);

    if (!ic2) return res.status(400).send("Invalid ic2.");

    console.log("bodyyyyyyyyyyyy", req.body);
    const client = await Client.findById(req.params.id);

    if (!client)
      return res
        .status(404)
        .send("The client with the given ID was not found.");

    // await client.set(_matricule)
    await client.set({
      descriptionClient: req.client.descriptionClient,
      quantite: req.client.quantite,
      // unite:req.client.fiche.unite,
    });
    console.log("client", client);
    await client.save();

    res.send(client);
  })
);

router.put(
  "/:id",
  [validateObjectId, validate(validateClient)],
  asyncMiddleware(async (req, res) => {
    const client = await Client.findByIdAndUpdate(
      req.params.id,
      { ...req.body },
      { new: true }
    );

    if (!client)
      return res
        .status(404)
        .send("The client with the given ID was not found.");

    res.send(client);
  })
);

router.delete(
  "/:id",
  validateObjectId,
  asyncMiddleware(async (req, res) => {
    const client = await Client.findByIdAndRemove(req.params.id);

    if (!client)
      return res
        .status(404)
        .send("The client with the given ID was not found.");

    res.status(200).json({ msgsrv: "deleted with success" });
  })
);

module.exports = router;
