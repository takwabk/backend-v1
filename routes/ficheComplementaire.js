const express = require("express");
const router = express.Router();
const { Fiche } = require("../models/ficheComplementaire");

const asyncMiddleware = require("../middleware/async");

router.post(
  "/fiches",

  asyncMiddleware(async (req, res) => {
    console.log(req.body);
    fiche = new Fiche(req.body);
    fiche = await fiche.save();
    res.send(fiche);
  })
);
router.post(
  "/updateValeur",

  async (req, res) => {
    fiche = new Fiche(req.body);
    fiche = await fiche.save();
    res.send(fiche);
  }
);

router.get(
  "/fiches",
  asyncMiddleware(async (req, res) => {
    const fiche = await Fiche.find().sort("IC1");
    //  result = JSON.parse(JSON.stringify(fiche))

    res.send(fiche);
  })
);
module.exports = router;
